var mongoose = require('mongoose');
var moment = require('moment');
var Schema = mongoose.Schema;

var reservaSchema = new Schema({
    desde: Date,
    hasta: Date,
    bicicleta: { type: mongoose.Schema.Types.ObjectId, ref: 'Bicicleta'},
    usuario: {type: mongoose.Schema.Types.ObjectId, ref: 'Usuario'}
});

reservaSchema.methods.diasDeReserva = function(){
    return moment(this.hasta).diff(moment(this.desde), 'days') + 1;
}

reservaSchema.statics.createInstance = function (desde, hasta, biciId, userId) {
    return new this(
        { 
            desde: desde,
            hasta: hasta,
            bicicleta: biciId,
            usuario: userId
    });
};

reservaSchema.statics.allReservas = function (cb) {
    return this.find({}).populate('bicicleta').populate('usuario').exec(cb);
};

reservaSchema.statics.add = function (reserva, cb) {
    this.create(reserva, cb);
};

reservaSchema.statics.findById = function (id, cb) {
    return this.findOne({_id: id}).populate('bicicleta').populate('usuario').exec(cb);
};

reservaSchema.statics.removeById = function (id, cb) {
    return this.deleteOne({_id: id}, cb);
};

module.exports = mongoose.model('Reserva', reservaSchema);