var mongoose = require('mongoose');
var Reserva = require('./reserva');
const Token = require('../models/token');
const mailer = require('../mailer/mailer');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const uniqueValidator = require('mongoose-unique-validator');
const { set } = require('../mailer/mailer');
const { countDocuments } = require('./reserva');
const { strict } = require('assert');
const saltRounsd = 10;

var Schema = mongoose.Schema;

//Funcion para validad email
const validateEmail = function(email){    
    const re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/ ;
    return re.test(email);
}

var usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        lowercase: true,
        unique: true,
        validate: [validateEmail, 'Por favor, ingrese un email valido'],
        match: [/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/]
    },
    password: {
        type: String,
        required: [true, 'El password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    },
    googleId : String,
    facebookId : String
});

//Se agrega plugin unique para el schema
usuarioSchema.plugin(uniqueValidator, {message: 'El {PATH} ya existe con otro usuario'});

//Encriptar el password ingresado
usuarioSchema.pre('save', function(next){
    if(this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRounsd);
    }
    next();
});

//Validar la encriptacion del password
usuarioSchema.methods.validPassword = function(password){
    return bcrypt.compareSync(password, this.password);
}


usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
    var reserva = new Reserva({
        usuario: this._id,
        bicicleta: biciId,
        desde: desde,
        hasta: hasta
    });
    reserva.save(cb);
}
usuarioSchema.statics.createInstance = function (nombre) {
    return new this({ nombre: nombre });
};

usuarioSchema.methods.toString = function () {
    return 'nombre: ' + this.nombre;
};

usuarioSchema.statics.allUsers = function (cb) {
    return this.find({}, cb);
};

usuarioSchema.statics.add = function (aUser, cb) {
    this.create(aUser, cb);
};

usuarioSchema.statics.findById = function (id, cb) {
    return this.findOne({_id: id},cb);
};

usuarioSchema.statics.removeById = function (id, cb) {
    return this.deleteOne({_id: id}, cb);
};

usuarioSchema.methods.enviar_email_bienvenida = function(cb){
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function(err){
        if(err) {return console.log(err.message)}

        const mailOptions = {
            from: process.env.EMAIL_FROM,
            to: email_destination,
            subject: 'Verificación de cuenta',
            text: 'Hola,\n\n' + 
            'Por favor, para verificar su cuenta haga click en el siguiente link: \n'+
            process.env.HOST +  '\/token/confirmation\/' + token.token
        };

        mailer.sendMail(mailOptions, function(err){
            if(err) {return console.log(err.message)}
            console.log('Se ha enviado un email de bienvenida a: ' + email_destination)
        });
    });
}

usuarioSchema.methods.resetPassword = function(cb){
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function(err){
        if(err) {return cb(err)}

        const mailOptions = {
            from: process.env.EMAIL_FROM,
            to: email_destination,
            subject: 'Reseteo de password de cuenta',
            text: 'Hola,\n\n' + 
            'Por favor, para resetear el password de su cuenta haga click en el siguiente link: \n'+
            process.env.HOST + '\/resetPassword\/' + token.token
        };

        mailer.sendMail(mailOptions, function(err){
            if(err) {return cb(err)}
            console.log('Se ha enviado un email para resetear el password a: ' + email_destination)
        });

        cb(null);
    });
}

usuarioSchema.statics.findOneOrCreatedByGoogle = function findOneOrCreate(condition, callback){
    const self = this;
    self.findOne({
        $or: [
            { 'googleId' : condition.id},{'email': condition.emails[0].value}
        ]
    }, (err, result) => {
            if(result){
                callback(err, result);
            }else{
                let values = {};
                values.googleId = condition.id;
                values.email = condition.emails[0].value;
                values.nombre = condition.displayName || 'SIN NOMBRE';
                values.verificado = true;
                values.password = condition.name.givenName + condition.name.familyName;
                // values.password = condition._json.etag;
                self.create(values, (err, result)=> {
                    if (err) {console.log(err)}
                    return callback(err, result);
                });
            }
        })
};

usuarioSchema.statics.findOneOrCreatedByFacebook = function findOneOrCreate(condition, callback){
    const self = this;
    self.findOne({
        $or: [
            { 'facebookId' : condition.id},{'email': condition.emails[0].value}
        ]
    }, (err, result) => {
            if(result){
                callback(err, result);
            }else{
                let values = {};
                values.facebookId = condition.id;
                values.email = condition.emails[0].value;
                values.nombre = condition.displayName || 'SIN NOMBRE';
                values.verificado = true;
                values.password = crypto.randomBytes(16).toString('hex');
                // values.password = condition._json.etag;
                self.create(values, (err, result)=> {
                    if (err) {console.log(err)}
                    return callback(err, result);
                });
            }
        })
};

module.exports = mongoose.model('Usuario', usuarioSchema);
