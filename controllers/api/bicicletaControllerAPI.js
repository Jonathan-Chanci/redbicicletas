var Bicicleta = require('../../models/bicicleta');

//listar Bicicletas
exports.bicicleta_list = function(req, res){
    Bicicleta.allBicis(function (err, bicis) {
        if(err) console.error(err);
        res.status(200).json({
            bicicletas: bicis
        });
     });    
}

//Crear bicicletas
exports.bicicleta_create = function(req, res){
    var bici = Bicicleta.createInstance(req.body.code, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lon];

    Bicicleta.add(bici);
    res.status(200).json({biciletas: bici});
}
// exports.bicicleta_create = function(req, res){
//     var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
//     bici.ubicacion = [req.body.lat, req.body.lon];

//     Bicicleta.add(bici);
//     res.status(200).json({biciletas: bici});
// }

//Delete
exports.bicicletas_delete = function(req, res){
    let code = req.params.id;
    Bicicleta.findByCode(code, function(err, bici){
        if(err) console.error(err);
        if(bici != null){
            Bicicleta.removeByCode(code, function(err, removedBici){
                if(err) console.error(err);                
                console.error(err);
                res.status(204).send();                              
            });
        }
        else
            res.status(500).send("La bicicleta con code: " + code + " no existe.");        
    });
}
// exports.bicicletas_delete = function(req, res){
//     let id = req.params.id;
//     let bici = Bicicleta.findById(id);
//     if(bici != null){
//         Bicicleta.removeById(req.params.id);
//         res.status(204).send();
//     }
//     else{
//         res.status(500).send();
//     }
// }

//Update
exports.bicicletas_update = function(req, res){
    let code = req.params.id;
    Bicicleta.findByCode(code, function(err, bici){
        if(err) console.error(err);
        if(bici != null){
            bici.color = req.body.color;
            bici.modelo = req.body.modelo;
            bici.ubicacion = [req.body.lat, req.body.lon];
            bici.save(function(err) {
                if (err) return console.error(err);
                res.status(200).json({ bicicleta: bici});
            });
        }
        else
            res.status(500).send("La bicicleta con code: " + code + " no existe.");
    });
}
// exports.bicicletas_update = function(req, res){
//     let id = req.params.id;
//     let bici = Bicicleta.findById(id);
//     bici.color = req.body.color;
//     bici.modelo = req.body.modelo;
//     bici.ubicacion = [req.body.lat, req.body.lon];

//     res.status(200).json({
//         bicicleta: bici
//     });
// }