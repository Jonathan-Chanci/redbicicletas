var Usuario = require('../../models/usuario');

exports.usuarios_list = function(req, res){
    Usuario.find({}, function(err, usuarios){
        res.status(200).json({usuarios: usuarios});
    });
}

exports.usuarios_create = function(req, res){
    var usuario = new Usuario({
        nombre: req.body.nombre,
        email: req.body.email,
        password: req.body.password
    });
    
    Usuario.create(usuario,function(err, newUser){
        if(err) res.status(500).json(err);
        if(newUser != null){
            newUser.enviar_email_bienvenida();
            res.status(200).json({newUser, message: 'Verifique su correo electronico para activar su cuenta'});
        }
    });
}

exports.usuario_reservar = function(req, res){
    Usuario.findById(req.body.id, function(err, usuario){
        usuario.reservar(req.body.bici_id, req.body.desde, req.body.hasta, function(err){
            res.status(200).send();
        });
    });
}