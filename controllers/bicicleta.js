var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function(req, res){
    Bicicleta.allBicis(function (err, bicis) {            
        res.render("bicicletas/index", {
            bicis: bicis
        });
     });
}

//Crear bicicletas
exports.bicicleta_create_get = function(req, res){
    res.render('bicicletas/create');
}

exports.bicileta_create_post = function(req, res){
    var aBici = new Bicicleta({
        code: req.body.code,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion : [req.body.lat, req.body.lon]
    });

    Bicicleta.add(aBici, function (err, newBici) {
        if(err) console.error(err);
        res.redirect("/bicicletas");
    });
}

exports.bicicleta_delete_post = function(req, res){
    Bicicleta.removeByCode(req.body.code, function(err, removedBici){
        if(err)console.error(err);
        res.redirect('/bicicletas');
    });
}
exports.bicicleta_update_get = function(req, res){
    Bicicleta.findByCode(req.params.id, function(err, bici){
        res.render('bicicletas/update', {bici});
    });
}
exports.bicicleta_update_post = function(req, res){
    var bici = Bicicleta.findByCode(req.params.id, function(err, bici){
        bici.code = req.body.code;
        bici.color = req.body.color;
        bici.modelo = req.body.modelo;
        bici.ubicacion = [req.body.lat, req.body.lon];
        bici.save(function(err) {
            if (err) return console.error(err);
            res.redirect("/bicicletas");
        });
    });
}
exports.bicicleta_detail_get = function(req, res){
    let bici = Bicicleta.findByCode(req.params.id, function(err, bici){
        res.render('bicicletas/detail',{bici});
    });
}