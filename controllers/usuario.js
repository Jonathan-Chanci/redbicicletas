var Usuario = require('../models/usuario');

exports.usuario_list = function(req, res){
    Usuario.allUsers(function (err, users) {            
        res.render("usuarios/index", {
            usuarios: users
        });
     });
}

//Crear usuario
exports.usuario_create_get = function(req, res){
    res.render('usuarios/create',{errors: {}, usuario: new Usuario()});
}

exports.usuario_create_post = function(req, res){
    if(req.body.pwd != req.body.confirm_pwd){
        res.render('usuarios/create', {
            errors: {
                confirm_password: {
                    message: 'No coincide con el password ingresado'
                }
            },
            usuario: new Usuario({
                nombre: req.body.nombre,
                email: req.body.email
            })
        });
        return;
    }
    var user = new Usuario({
        nombre: req.body.nombre,
        email: req.body.email,
        password: req.body.pwd
    });

    Usuario.create(user, function(err, nuevoUsuario){
        if(err){
            res.render('usuarios/create',{
                errors: err.errors,
                usuario: new Usuario({
                    nombre: req.body.nombre,
                    email: req.body.email
                })
            });
        }
        else{
            nuevoUsuario.enviar_email_bienvenida();
            res.redirect('/usuarios');
        }
    });
}

//Creat cuenta nueva
exports.usuario_create_get_Account = function(req, res){
    res.render('session/createAccount',{errors: {}, usuario: new Usuario()});
}

exports.usuario_create_post_Account = function(req, res){
    if(req.body.pwd != req.body.confirm_pwd){
        res.render('session/createAccount', {
            errors: {
                confirm_password: {
                    message: 'El password no coincide'
                }
            },
            usuario: new Usuario({
                nombre: req.body.nombre,
                email: req.body.email
            })
        });
        return;
    }
    var user = new Usuario({
        nombre: req.body.nombre,
        email: req.body.email,
        password: req.body.pwd
    });

    Usuario.create(user, function(err, nuevoUsuario){
        if(err){
            res.render('session/createAccount',{
                errors: err.errors,
                usuario: new Usuario({
                    nombre: req.body.nombre,
                    email: req.body.email
                })
            });
        }
        else{
            nuevoUsuario.enviar_email_bienvenida();
            res.render('session/login',{succ: { message : "Cuenta creada con exito, logueate!"}, warning: { message: "Recuerda ingresar a tu correo para verificar la cuenta"}});
        }
    });
}

exports.usuario_delete_post = function(req, res){
    Usuario.removeById(req.body.id, function(err, removedUser){
        if(err)console.error(err);
        res.redirect('/usuarios');
    });
}

exports.usuario_update_get = function(req, res){
    Usuario.findById(req.params.id, function(err, usuario){
        res.render('usuarios/update', {errors: {}, usuario});
    });
}
exports.usuario_update_post = function(req, res){
    var update_values = {nombre: req.body.nombre};
    Usuario.findByIdAndUpdate(req.params.id, update_values, function(err, user){
        if(err){
            res.render('usuarios/update', {
                errors: err.errors, 
                usuario: new Usuario({
                    nombre: req.body.nombre,
                    email: req.body.email
                })
            });
        }
        else{
            res.redirect('/usuarios');
            return;
        }
    });
}

exports.usuario_detail_get = function(req, res){
    Usuario.findById(req.params.id, function(err, user){
        res.render('usuarios/detail',{user});
    });
}