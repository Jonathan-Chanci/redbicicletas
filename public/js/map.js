var mymap = L.map('mapid').setView([6.24738747, -75.56628227], 13);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);

//Marcadores
//L.marker([6.24585169, -75.57510138]).addTo(mymap);
//L.marker([6.24235351, -75.59269667]).addTo(mymap);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(bici => {
            L.marker(bici.ubicacion,{title: bici.id}).addTo(mymap);
        });
    }
});