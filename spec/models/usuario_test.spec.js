var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var Usuario = require('../../models/usuario');
var Reserva = require('../../models/reserva');

describe('Testing Usuarios', () => {
    //Metodo que se ejecuta antes de todos los test
    beforeAll((done) => { mongoose.connection.close(done) });

    //Metodo que se ejecuta antes de cada test
    beforeAll(function (done) {
        mongoose.disconnect();
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error:'));
        db.once('open', function() {
            console.log('Conexion exitosa a la base de datos');
            done();
        });
    });

    //Metodo que se ejecuta despues de cada test
    afterEach(function (done) {
        Reserva.deleteMany({}, function(err, success){
           if(err) console.log(err);
            Usuario.deleteMany({},  function(err, success){
                if(err) console.log(err);
                Bicicleta.deleteMany({}, function (err, success) {
                    if(err) console.log(err);
                    done(); 
                 });
            });
        });        
    });

    describe('Cuando un Usuario reserva una bici', () =>{
        it('Debe existir la reserva', (done) => {
            const usuario = new Usuario({nombre : 'Jonathan'});
            usuario.save();
            const bicicleta = new Bicicleta({code: 1, color: 'verde', modelo: 'Urbana'});
            bicicleta.save();

            var hoy = new Date();
            var mañana = new Date().setDate(hoy.getDate() + 1);
            usuario.reservar(bicicleta.id, hoy, mañana, function(err, reserva){
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas){
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
        });
    });
});