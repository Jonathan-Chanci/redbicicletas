var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function () {
    //Metodo que se ejecuta antes de todos los test
    beforeAll((done) => { mongoose.connection.close(done) });

    //Metodo que se ejecuta antes de cada test
    beforeAll(function (done) {
        mongoose.disconnect();
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error:'));
        db.once('open', function() {
            console.log('Conexion exitosa a la base de datos');
            done();
        });
    });

    //Metodo que se ejecuta despues de cada test
    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
           if(err) console.log(err);
           done(); 
        });
    });

    //Test Create
    describe('Bicicleta.createInstance', () => {
        it('Crea una instancia de Bicicleta', () => {
            var bici = Bicicleta.createInstance(1,'Blanco', 'Urbana', [444,444]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe('Blanco');
            expect(bici.modelo).toBe('Urbana');
            expect(bici.ubicacion[0]).toEqual(444);
            expect(bici.ubicacion[1]).toEqual(444);
        });
    });

    //AllBicis
    describe('Bicicleta.allBicis', () => {
        it('Comienza vacia', (done) => {
            Bicicleta.allBicis(function (err, bicis) {
               expect(bicis.length).toBe(0);
               done(); 
            });
        });
    });

    //Agregar
    describe('Bicicleta.add', () =>{
        it('agrega solo una bici', (done) =>{
            var aBici = new Bicicleta({
                code: 1,
                color: "verde",
                modelo: "urbana"
            });
            Bicicleta.add(aBici, function (err, newBici) {
                if(err) console.log(err);
                Bicicleta.allBicis(function (err, bicis) {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();                    
                });
            })
        });
    });

    //Find by code
    describe('Bicicleta.findByCode', () => {
        it('debe devolver la bici con el code 1', function (done) {
            Bicicleta.allBicis(function (err, bicis) {
               //se evalua que no existan bicis
                expect(bicis.length).toBe(0);
                //Se instancia un objeto schema bicicleta
                var aBici = new Bicicleta({code: 1, color: "verde", modelo:"urbana"});
                //Se agrega
                Bicicleta.add(aBici, function (err, newBici) {
                    if (err) console.log(err);

                    var aBici2 = new Bicicleta({code: 2, color: "roja", modelo:"montaña"});
                    Bicicleta.add(aBici2, function (err, newBici) {
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function (err, targetBici) {
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);
                            done();
                        });
                    });
                });
            });
        });
    });

    //Delete
    describe('Bicicleta.remobeByCode', () =>{
        it('debe devolver 0', function (done) {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);
                //Se instancia un objeto schema bicicleta
                var aBici = new Bicicleta({code: 1, color: "verde", modelo:"urbana"});
                Bicicleta.add(aBici, function (err, newBici) {
                    if(err) console.log(err);
                    Bicicleta.removeByCode(1, function (err, deleteBici) {
                        if(err) console.log(err);
                        Bicicleta.allBicis(function (err, bicis) {
                            expect(bicis.length).toBe(0);
                            done();
                        });
                    });
                });
            });
        });
    });
});



/*
//Antes de cada test ejecutar
beforeEach(() => { Bicicleta.allBicis = []; });

//Coleccion de bicicletas
describe('Biclicletas.allBicis', () => {
    it('Comieza vacio', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    })
});

//Añadir bicicleta
describe('Bicicletas.add', () => {
    it('Agregar una', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = Bicicleta(1,'rojo','montaña',[111,1111]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

//Busqueda por id
describe('Bicicleta.findById',() =>{
    it('Debe devolver la bici con id 1', () =>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1,'rojo','montaña',[111,1111]);
        var b = new Bicicleta(2,'rojo','montaña',[111,1111]);
        Bicicleta.add(a);
        Bicicleta.add(b);

        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(a.color);
        expect(targetBici.modelo).toBe(a.modelo);
    });
});

//Eliminación por id
describe('Biclicleta.removeById', () => {
    it('Debe devolver 0', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1,'rojo','montaña',[111,1111]);
        Bicicleta.add(a);
        Bicicleta.removeById(1);
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});*/
