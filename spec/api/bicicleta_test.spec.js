var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

const url = 'http://localhost:3000/api/bicicletas';

describe('Bicicletas API Test', () =>{
    //Metodo que se ejecuta antes de todos los test
    beforeAll((done) => { mongoose.connection.close(done) });

    //Metodo que se ejecuta antes de cada test
    beforeAll(function (done) {
        mongoose.disconnect();
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error:'));
        db.once('open', function() {
            console.log('Conexion exitosa a la base de datos');
            done();
        });
    });

    //Metodo que se ejecuta despues de cada test
    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
           if(err) console.log(err);
           done(); 
        });
    });

    //Get - listar bicicletas
    describe('Get bicicletas', () => {
        it('status 200', (done) => {
            request.get(url, function(error, response, body){          
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas).toEqual([]);
                done();
            });
        });
    });

    //Create
    describe('Post bicicletas', () => {
        it('status 200', (done) => {            
            var headers = {'content-type' : 'application/json'};
            var aBici = `{
                "code": 1,
                "color": "Azul",
                "modelo": "montaña",
                "lat": 6.27025299,
                "lon": -75.5585146
            }`;

            request.post({
                headers : headers,
                body : aBici,
                url : url + '/create'
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).biciletas;
                expect(bici.color).toBe("Azul");
                expect(bici.ubicacion[0]).toBe(6.27025299);
                expect(bici.ubicacion[1]).toBe(-75.5585146);
                done();
            });
        });
    });

    //Delete
    describe('Delete bicicleta', () => {
        it('Status 204', (done) => {
            var bici = Bicicleta.createInstance(1, "Azul", "montaña");
            Bicicleta.add(bici);
            request.delete(url + "/delete/" + bici.code, function(error, response, body){
                expect(response.statusCode).toBe(204);
                done();
            });
        });
    });

    //Update
    describe('Put bicicletas', () => {        
        it('status 200', (done) => {
            var bici = Bicicleta.createInstance(1, "Azul", "montaña");
            Bicicleta.add(bici);
            var headers = {'content-type' : 'application/json'};
            var aBici = `{
                "color": "Verde",
                "modelo": "montaña",
                "lat": 6.27025299,
                "lon": -75.5585146
            }`;

            request.put({
                headers : headers,
                body : aBici,
                url : url + "/update/" + bici.code
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                Bicicleta.findByCode(bici.code, function(err , bici){
                    expect(bici.color).toBe("Verde");
                    done();
                });
            });
        });
    });
});



/*
//Antes de cada test ejecutar
beforeEach(() => {
    Bicicleta.allBicis = [];
});

describe('Bicicletas API', () => {
    //Get bicicletas
    describe('Get bicicletas', () => {
        it('status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, 'rojo', 'montaña');
            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas', function(error, response, body){
                expect(response.statusCode).toBe(200);
            });
        });
    });
    // Crear bicicleta
    describe('Post bicicletas', () => {
        it('status 200', (done) => {            
            var headers = {'content-type' : 'application/json'};
            var aBici = `{
                "id": 1,
                "color": "Azul",
                "modelo": "montaña",
                "lat": 4444,
                "lon": 4444
            }`;

            request.post({
                headers : headers,
                body : aBici,
                url : 'http://localhost:3000/api/bicicletas/create'
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(1).color).toBe("Azul");
                done();
            });
        });
    });

    //Actualizar bicicleta
    describe('Put bicicletas', () => {        
        it('status 200', (done) => {
            var a = new Bicicleta(1, 'rojo', 'montaña');
            Bicicleta.add(a);
            var headers = {'content-type' : 'application/json'};
            var aBici = `{
                "color": "Azul",
                "modelo": "montaña",
                "lat": 4444,
                "lon": 4444
            }`;

            request.put({
                headers : headers,
                body : aBici,
                url : 'http://localhost:3000/api/bicicletas/update/1'
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(1).color).toBe("Azul");
                done();
            });
        });
    });

    //Eliminar bicicleta
    describe('Delete bicicletas', () => {
        it('status 204', (done) =>{
            var a = new Bicicleta(1, 'rojo', 'montaña');
            Bicicleta.add(a);
            request.delete('http://localhost:3000/api/bicicletas/delete/1', function(error, response, body){
                expect(response.statusCode).toBe(204);
                done();
            });
        });
    });


});

*/