require('newrelic');
require('dotenv').config();
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const passport = require('./config/passport');
const session = require('express-session');
const jwt = require('jsonwebtoken');
const MongoDBStore = require('connect-mongodb-session')(session);

var indexRouter = require('./routes/index');
//ruta de aplicaciones nuevas
var usersRouter = require('./routes/users');
var bicicletaRouter = require('./routes/bicicletas');
var usuariosRouter = require('./routes/usuarios');
var bicicletasAPIRouter = require('./routes/api/bicicletasRoutesAPI');
var usuariosAPIRouter = require('./routes/api/usuariosRoutesAPI');
var reservasRouter = require('./routes/reservas');
var tokenRouter = require('./routes/token');
var authAPIRouter = require('./routes/api/auth');

//Required Modelos
const Usuario = require('./models/usuario');
const Token = require('./models/token');

//la session se almacenara en memoria cache
//cuando se detenga el servidor se borrara toda
let store;
if(process.env.NODE_ENV === 'development'){
  store = new session.MemoryStore;
}else{
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: 'sessions'
  });
  store.on('error', function(error){
    assert.ifError(error);
    assert.ok(false);
  });
}

var app = express();

//Agregar secret para la validacion por token
app.set('secretKey', 'jwt_pwt_1234567890');

//Configuracion de la session que se almacenara en cache
app.use(session({
  //240 horas
  cookie: {maxAge: 240 * 60 * 60 * 1000},
  store: store,
  saveUninitialized: true,
  resave: 'true',
  secret: 'red_bicis_1234567890'
}));

//***************************************CONFIGURACION DE MONGOOSE************************************************ */
var mongoose = require('mongoose');
const { request } = require('express');
const { assert } = require('console');
//var mongoDB = 'mongodb://localhost/red_bicicletas';
// var mongoDB = 'mongodb+srv://admin:Admin123456@red-bicicletas.dl8nz.mongodb.net/red-bicicletas?retryWrites=true&w=majority';
//Accede a variables de entorno para tomar la url de la base de datos
var mongoDB = process.env.MONGO_URI;
mongoose.connect(mongoDB, {useNewUrlParser: true});
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
//***************************************FIN CONFIGURACION DE MONGOOSE************************************************ */

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
//usar la session en el servidor
app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(path.join(__dirname, 'public')));


//***************************************METODOS PARA LOGUEO Y CONTRASEÑA************************************************ */
//redireccion a login
app.get('/login', function(req, res){
  res.render('session/login');
});

app.post('/login',function(req, res, next){
  passport.authenticate('local', function(err, usuario, info){
    if(err) return next(err);
    if(!usuario) return res.render('session/login', {info});
    req.logIn(usuario, function(err){
      if(err) return next(err);
      return res.redirect('/');
    });
  })(req, res, next);
});

app.get('/logout', function(req, res){
  req.logOut();
  res.redirect('/login');
});

app.get('/forgotPassword', function(req, res){
  res.render('session/forgotPassword');
});
app.post('/forgotPassword', function(req, res){
  Usuario.findOne({email: req.body.email}, function(err, usuario){
    if(!usuario) return res.render('session/forgotPassword', {info: {
        message: 'No existe el email para un usuario existente.'
    }});

    usuario.resetPassword(function(err){
      if(err) return next(err);
      console.log('session/forgotPasswordMessage');
    });

    res.render('session/forgotPasswordMessage');
    
  });
});

app.get('/resetPassword/:token', function(req, res, next){
    Token.findOne({token: req.params.token}, function(err, token){
        if(!token) return res.status(400).send({
          type: 'not-verifiel', 
          msg: 'No existe un usuario asociado al token. verifique que su token no haya expirado'
        });

        Usuario.findById(token._userId, function(err, usuario){
            if(!usuario) return res.status(400).send({
              msg: 'No existe un usuario asociado al token.'
            });
            res.render('session/resetPassword', {errors: {}, usuario: usuario});
        });
    });
});

app.post('/resetPassword', function(req, res){
    if(req.body.password != req.body.confirm_password){
      res.render('session/resetPassword', {errors: {
          confirm_password : {
            message: 'No coincide con el password ingresado.'
          }
        },
        usuario: new Usuario({email: req.body.email})
      });
      return;
    }
    Usuario.findOne({email: req.body.email}, function(err, usuario){
        usuario.password = req.body.password;
        usuario.save(function(err){
          if(err){
            res.render('session/resetPassword',{
              errors: err.errors,
              usuario: new Usuario({email: req.body.email})
            });
          }else{
            res.render('session/login', {succ : {message: 'Password reseteado correctamente.'}});
          }
        })
    });
});



//Configuracion para autenticación con google
app.get('/auth/google', passport.authenticate('google',{
    scope: [
       'email',
       'profile'
      // 'https://www.googleapis.com/auth/plus.login',
      // 'https://www.googleapis.com/auth/plus.profile.emails.read'
    ]
  }));

app.get('/auth/google/callback', passport.authenticate('google',{
    successRedirect: '/',
    failureRedirect: '/error'
  })
);

//***************************************FIN METODOS PARA LOGUEO Y CONTRASEÑA************************************************ */

//***************************************USO DE RUTAS EN LA APLICACION************************************************ */
//uso de la autenticacion por api
app.use('/api/auth', authAPIRouter);
app.use('/', indexRouter);
app.use('/users', usersRouter);
//al ingresar a bicicletas verifica si esta logueado
//si esta logueado continua con biciclietaRouter
//sino redirecciona a login
app.use('/bicicletas', loggedIn, bicicletaRouter);
app.use('/usuarios', usuariosRouter);

//validacion de api con jwt
app.use('/api/bicicletas', validarUsuario, bicicletasAPIRouter);
app.use('/api/usuarios', usuariosAPIRouter);
app.use('/reservas',loggedIn, reservasRouter);
app.use('/token', tokenRouter);

app.use('/privacy_policy', function(req, res){
  res.sendFile('public/privacy_policy.html');
});
//Verificacion de dominio
app.use('/google6c2b685481162fb2', function(req, res){
  res.sendFile('public/google6c2b685481162fb2.html');
});


//***************************************FIN USO DE RUTAS EN LA APLICACION************************************************ */


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

//***************************************METODOS PARA VALIDACIONES MIDDLEWARE************************************************ */
//funcion que verifica si el usuario esta logueado
function loggedIn(req, res, next){
  if(req.user){
    next();
  }else{
    console.log('usuario sin loguearse');
    res.redirect('/login');
  }
}

function validarUsuario(req, res, next){
  jwt.verify(req.headers['x-access-token'],  req.app.get('secretKey'), function(err, decoded){
    if(err){
      res.json({status: 'error', message: err.message, data: null});
    }else{
      req.body.userId = decoded.id;
      console.log('jwt verify: '+ decoded);
      next();
    }
  });
}
//*************************************** FIN METODOS PARA VALIDACIONES MIDDLEWARE************************************************ */
module.exports = app;
