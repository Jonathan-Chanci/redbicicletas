# MIBICI

Red de bicicletas en mi ciudad

## Instalación

Usa el siguiente comando para descargar las dependencias del proyecto.

```bash
npm install
```

## Usa este comando para ejecutar el proyecto

```bash
npm run devstart
```

## Usa este comando para probar todos los test con jasmine

```bash
npm test
```

## Estas son las url para la API Biciletas

| Acción | URL |
| ------ | ------ |
| Listar(Get) | [http://localhost:3000/api/bicicletas] |
| Crear(Post) | [http://localhost:3000/api/bicicletas/create]|
| Eliminar(Delete) | [http://localhost:3000/api/bicicletas/delete/id]|
| Actualizar(Put) | [http://localhost:3000/api/bicicletas/update/id]|

## Estas son las url para la API Usuarios

| Acción | URL |
| ------ | ------ |
| Listar(Get) | [http://localhost:3000/api/usuarios] |
| Crear reserva(Post) | [http://localhost:3000/api/usuarios/reservar]|
| Crear Usuario(Post) | [http://localhost:3000/api/usuarios/create]|

## Estas son las url para la API de atenticación de Usuarios

| Acción | URL |
| ------ | ------ |
| Autenticarse(Post) | [http://localhost:3000/api/auth/authenticate] |
| Reset password(Post) | [http://localhost:3000/api/auth/forgotpassword]|


## License
[MIT](https://choosealicense.com/licenses/mit/)